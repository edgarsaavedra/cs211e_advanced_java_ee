import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;


public class ReflectionJ
{
	private String var;
	public ReflectionJ(String str)
	{
		var = str;
	}
	public String getStr()
	{
		return var;
	}
	public static void main(String[] args)
  {
		try
    {
			//get class by name
	    Class test = Class.forName("ReflectionJ");
//	    System.out.println(test.);
//	    System.out.println(new ReflectionJ("test").getClass().getName());
	    //create object of constructor
	    System.out.println(test.getMethods()[0].getName());
	    Constructor<?> cons[] = test.getConstructors();

    	System.out.println(cons.length);
	    for(Constructor<?> test2 : cons)
	    {
	    	System.out.println(test2.getName());
	    }
	    Constructor<?> con = test.getConstructors()[0];
	    try
      {
	      ReflectionJ o = (ReflectionJ) con.newInstance(
	      		new Object[]{"our new object"});
	      System.out.println(o.getStr());
      }
      catch (InstantiationException e)
      {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }
      catch (IllegalAccessException e)
      {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }
      catch (InvocationTargetException e)
      {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }
    }
    catch (ClassNotFoundException e)
    {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
    catch (IllegalArgumentException e)
    {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
  }
}
