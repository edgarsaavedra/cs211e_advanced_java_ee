/*
 Author: Edgar Saavedra
 Date  : 3/15/16
 Program Name: RmiServerImpl.java
 Homework: 3
 Objective: This program serves 
 as an API to connect to Oracle/SQL
 databases and connect to 
 an RMI server.
*/
import java.net.*;
import java.rmi.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.sql.*;
import java.util.*;

public class RmiServerImpl extends UnicastRemoteObject 
implements RmiServerIntf 
{
	private Properties p;	
	private Connection con;
	private DBConnector connection;		
	private USStateParser parser;
	private	ArrayList<String> states;
	private	ArrayList<String> capitals;

	 //****************************RmiServerImpl()*********************
	protected RmiServerImpl() throws RemoteException
	{
		super();
		p = System.getProperties();

		  if(p.getProperty("dbType") != null)
	    {
        String filepath = "";
        if(p.getProperty("dbType").equals("mysql"))
        {
          filepath = "/Users/edgar/Google Drive/CCSF-Edgar/2016/"+
            "spring/cs211e_advanced_java_ee/Homework2/src/US_states";
          parser = new USStateParser(filepath);
        }
        if(p.getProperty("dbType").equals("oracle"))
        {
          filepath = "/students/esaaved2/public_html/"+
          		"cs211e/US_states";
          parser = new USStateParser(filepath);
        }
        if(parser != null)
        {
          Map collection = parser.parseUSStatesArr();
          collection = parser.parseUSStatesArr();
          capitals = (ArrayList<String>) collection.get("capitals");
          states   = (ArrayList<String>) collection.get("states");
      			//Handle our wanted connections
          connection = new DBConnector(p.getProperty("dbType"));
          if(connection != null)
          {
            System.out.println("db connection started");
            //start a connection
            con = connection.startCon();
            if(p.getProperty("createTable") != null)
              connection.createTable(con);
            if(p.getProperty("populate") != null)
              connection.populateTables(states, capitals, con);
          }
        }
			}else
			{
				con 		 = null;
				parser 	 = null;
				capitals = null;
				states   = null;
			}
	}

	 //****************************getState()**************************
	@Override
  public String[] getState(String capitalRegex)
  {
		ArrayList<String> capital = new ArrayList<>();
		ResultSet result = connection.getValueRegex(con,"States","state", 
				"capital",capitalRegex);
		try
    {
	    while(result.next())
	    {
	    	capital.add((String)result.getString(1));
	    }
	    String[] stockArr = new String[capital.size()];
	    stockArr = capital.toArray(stockArr);
	    return stockArr;
    } catch (SQLException e)
    {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
	  return null;
  }

	 //****************************getCapital()************************
	@Override
  public String[] getCapital(String stateRegex)
  {
		ArrayList<String> state = new ArrayList<>();
		ResultSet result = connection.getValueRegex(con,"States",
				"capital", "state",stateRegex);
		try
    {
	    while(result.next())
	    {
	    	state.add((String)result.getString(1));
	    }
	    String[] stockArr = new String[state.size()];
	    stockArr = state.toArray(stockArr);
	    return stockArr;
    } catch (SQLException e)
    {
	    // TODO Auto-generated catch block
	    e.printStackTrace();
    }
	  return null;
  }

	 //****************************closeConnection()*******************
	public void closeConnection()
	{
		if(con != null)
	    try
      {
	      con.close();
      } catch (SQLException e)
      {
	      // TODO Auto-generated catch block
	      e.printStackTrace();
      }
	}

	 //****************************main()*****************************
  public static void main(String[] args) 
  {
	  if(System.getSecurityManager() == null)
	  {
		  System.setSecurityManager(null);
	  }
	  try
	  {
	  		RmiServerImpl asi = new RmiServerImpl();
	       LocateRegistry.createRegistry(6600);
	       Naming.rebind("rmi://localhost:6600/GetUSInfo", asi);
	       System.out.println("GetUSInfo Ready!");
	       
	  }catch(Exception e){
	  	e.printStackTrace();
	  }
  }  

}
