/*
 Author: Edgar Saavedra
 Date  : 3/15/16
 Program Name: RmiServerIntf.java
 Homework: 3
 Objective: This program serves 
 as an API to connect to Oracle/SQL
 databases and connect to 
 an RMI server.
*/
import java.rmi.*;
public interface RmiServerIntf extends Remote 
{
	 //****************************capitalRegex()**********************
  public String[] getState(String capitalRegex) 
  		throws RemoteException;
	 //****************************stateRegex()************************
  public String[] getCapital(String stateRegex) 
  		throws RemoteException;
	 //****************************closeConnection()*******************
	public void closeConnection() throws RemoteException;
}
