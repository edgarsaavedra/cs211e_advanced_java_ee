/*
 Author: Edgar Saavedra
 Date  : 3/15/16
 Program Name: RmiServerImpl.java
 Homework: 3
 Objective: This program serves 
 as a client to connect to Oracle/SQL
 databases and connect to 
 an RMI server.
*/
import java.net.*;
import java.rmi.*;
public class RmiClient
{
	 //****************************main()*****************************
	public static void main(String[] args)
	{
		String url = "rmi://localhost:6600/GetUSInfo";
		try 
		{
			RmiServerIntf asi = (RmiServerIntf) Naming.lookup(url);
			// this will return all of the capitals where state 
			//begin with letter s 
			String state1 = "^S";
			System.out.println(state1+" all of the capitals where"+
					" state begin with letter s :");
			String[] strArr = asi.getCapital(state1);
			for(String str : strArr)
			{
				System.out.println(""+str);
			}
			//get capital were state begins with letter c
			String cap1 = "^C";
			System.out.println(cap1+", get capital were state "+
			"begins with letter c:");
			strArr = asi.getCapital(cap1);
			for(String str : strArr)
			{
				System.out.println(""+str);
			}
			//give capital where the state has 5 characters
			String cap2 = "^.....$";
			System.out.println(cap2+", get capital were the state "+
			"has 5 characters:");
			strArr = asi.getCapital(cap2);
			for(String str : strArr)
			{
				System.out.println(""+str);
			}
			
		} catch (MalformedURLException e) 
		{
			e.printStackTrace();
		} catch (RemoteException e) 
		{
			e.printStackTrace();
		} catch (NotBoundException e) 
		{
			e.printStackTrace();
		}
	}
}
