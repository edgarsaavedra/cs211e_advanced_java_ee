import java.io.*;
import java.net.*;

public class WhoisClient {
	public static void main(String[] args) {
		int c;
		try(Socket s = new Socket("whois.internic.net",43))
		{
			InputStream in = s.getInputStream();
			OutputStream out = s.getOutputStream();
			//if command line that we are using has any args use them
			//the needs to be a new line else it will pause.
			String str = (args.length == 0?"ccsf.edu":args[0]+"\n");
			//create a buffer
			byte buf[] = str.getBytes();//what ever domain convert to bytes
			while((c=in.read()) != -1 )
			{
				System.out.println((char)c);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
