import java.io.*;
import java.net.*;
import java.util.Scanner;

public class EchoUpperServer 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		try(ServerSocket s = new ServerSocket(16001))
		{
			while(true)
			{
				// important: waiting until a client connects
				Socket s1 = s.accept();
				//multi-threading, create a new tread and assign a job to the thread
				Runnable r = new EchoUpperHandler(s1);
				Thread t = new Thread(r);
				t.start();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public static class EchoUpperHandler implements Runnable
	{
		private Socket s;
		EchoUpperHandler(Socket i)
		{
			s = i;
		}
		@Override
		public void run() 
		{
			try(InputStream in = s.getInputStream())
			{
				//getting data from user
				try(OutputStream out = s.getOutputStream())
				{
					//make scanner
					try(Scanner sc = new Scanner(in))
					{
						//get the data
						// true in constructor means flush data out
						// dont put into memory or use buffer
						PrintWriter pw = new PrintWriter(out,true);
						pw.println("Enter Bye to Exit");
						boolean done = false;
						//as long as we have data
						while(!done && sc.hasNext())
						{
							String line = sc.nextLine();
							pw.println("Echo: "+line.toUpperCase());
							if(line.trim().equals("Bye"))
								done = true;
						}
					}catch (Exception e) { //IOException
					}
				}catch (IOException e) 
				{
				}
			}catch (IOException e) 
			{
			}
		}
		
	}

}
