import java.io.*; //io
import java.net.*; //network
import java.util.*; //scanner

public class Client 
{
	public static void main(String[] args) 
	{
		// to use ip we can convert to ip using IdleAddress (on hills)
		// socket class has an over loaded class to
		try(Socket s = new Socket("time-A.timefreq.bldrdoc.gov",13))
		{
			// once connection is made we are able to 
			//get and receive data. We create an input stream
			//getInputStream is a method of the socket class
			// using putInputStream we are able to send data with the socket
			InputStream is = s.getInputStream();
			
			// using a scanner is really easy to read data
			Scanner sc = new Scanner(is);
			//loop over scanner
			while(sc.hasNextLine())//as long as data
			{
				System.out.println(sc.nextLine());
			}
			// since we put the socket into try we dont need to stop it
			// using s.close();
			// like wise we dont have to worry agout input stream
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
