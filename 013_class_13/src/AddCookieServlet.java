

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AddCookieServlet
 */
@WebServlet("/AddCookieServlet")
public class AddCookieServlet extends HttpServlet {
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddCookieServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		response.getWriter().append("Served at: ").append(request.getContextPath());
		doPost(request,response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String data = request.getParameter("data");
		
		if( data == null )
			data = "its null";
		if( data.equals("") )
			data = "empty";
		
		/**
		 * now create a cookie
		 */
		Cookie cookie = new Cookie("MyCookie",data);
		// we can add the attributes
		cookie.setMaxAge(60);
		cookie.setVersion(0);
		
		
		//add cookie is a method of httpresponse
		//this is the one that adds the cookie for us
		// the cookie is created
		response.addCookie(cookie);
		
		response.setContentType("text/html");
		PrintWriter pw = response.getWriter();
		pw.println("<html><body>");
		pw.println("My cookie has been set to: ");
		pw.println(data);

		pw.println("<br><a href='/013_class_13/get-cookie'>See cookies</a>");
		
		
		pw.println("</body></html>");
		pw.close();
		
		
	}

}
