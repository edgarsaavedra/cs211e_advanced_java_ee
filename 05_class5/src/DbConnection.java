import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.mysql.jdbc.DatabaseMetaData;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSetMetaData;


public class DbConnection {

//	static boolean debug_mode = (System.getProperty("DEBUG")!=null);
	static boolean debug_mode = true;
	public static void debug(Object o) 
	{
		if(debug_mode)
		{
			System.out.println(""+o);
		}
	}
	public static void createTable(Connection con,String tableName,String fieldName,String fieldType)
	{
		String sql = "create table "+tableName+" ("+fieldName+" "+fieldType+")";//sql command
		try {
			Statement stmt1 = con.createStatement();
			stmt1.execute(sql);
			stmt1.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void runQuery(Connection con,String sql)
	{
		try {
			Statement stmt1 = con.createStatement();
			stmt1.execute(sql);
			stmt1.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

        try {
    		Class.forName("com.mysql.jdbc.Driver").newInstance();
    		//localhost
    		String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3306/cs211e";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
			Connection con = DriverManager.getConnection(url, userName, password);
			if(con != null)
			{
				String sql = "INSERT INTO emp (lastnme) VALUES ('Jones','wong','lin','green')";
//				runQuery(con,sql);
				
//				createTable(con,"emp","lastnme","varchar(225)");
				testMetaData(con);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static void testMetaData(Connection con)
	{
		try {

			debug("connected to the database");
			Statement stmt = con.createStatement();
			
			DatabaseMetaData dmd = (DatabaseMetaData) con.getMetaData();
			
			ResultSet rs = dmd.getTypeInfo();
			while(rs.next())
			{
				//get all of the names of al of the accepted types in database
//				debug(rs.getString("TYPE_NAME"));
			}
			
			String tables[]  = {"TABLE"};
			rs = dmd.getTables(null, null, "%", tables);
			while(rs.next())
			{
				// learn of the names of the column names
//				debug(rs.getString("TABLE_NAME"));
			}
			//this will give you more info about object
			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();
			// the number of columns
//			debug(rsmd.getColumnCount());
			// the name of the column
//			debug(rsmd.getColumnLabel(3));
			
			// "%" all column names or we could put a wild card to narrow down path aka this is a filter
			rs = dmd.getColumns(null, null, "Greetings", "%");
			while(rs.next())
			{
//				debug(rs.getType());
			}
			
			//this is academic approach - this approach puts alot of problems on system
			//we have an array of names
			String lnames[] = {"Jones","wong","lin","green"};
			for(String lname:lnames)
			{
//				rs = stmt.executeQuery("select * from emp where lastnme="+lname);
			}
			
			//instead use a prepared statement
			String query = "select * from emp"+" where lastnme=?";
			PreparedStatement ps = (PreparedStatement) con.prepareStatement(query);
			for(String lname : lnames)
			{
				//1 here refers to the first question mark, 
				//if we had 2  we would put 2 to get to the second question mark
				// we are putting everything into ps
				ps.setString(1, lname);
			}
			//by one call send the whole bundle we have made
			//this is much faster since only one call is involved
			rs = ps.executeQuery();
			//process the result
			while(rs.next())
			{
				debug(rs.getString("lastnme"));
			}
			
			stmt.close();
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
