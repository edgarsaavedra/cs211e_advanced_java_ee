import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Properties;

public class GetProperties
{
	//user.dir
	public static void main(String[] args)
	{
		generateXML();
	}
	public static void generateXML()
	{
		Properties p = System.getProperties();
		Enumeration e = p.propertyNames();
		if(e.hasMoreElements())
		{

			try
			{
				PrintWriter pw = new PrintWriter(p.getProperty("user.dir")+"/src/java-props.xml","UTF-8");
				
				pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
				pw.println("<java>");
				while(e.hasMoreElements())
				{
					String propName = e.nextElement().toString();
					if(propName.indexOf("java") > -1)
					{
						pw.print("<"+propName+">");
						pw.print(p.getProperty(propName));
						pw.print("</"+propName+">\n");
					}
				}
				pw.println("</java>");
				pw.close();
			}
			catch (FileNotFoundException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			catch (UnsupportedEncodingException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
}
