import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import javax.xml.parsers.*;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

public class XmlMethods
{

	public static void main(String[] args)
	{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		Properties p = System.getProperties();
		Enumeration e = p.propertyNames();
		if(e.hasMoreElements())
		{

			try
			{
				DocumentBuilder db = dbf.newDocumentBuilder();
				File ff =  getStateXMLFile();
				Document doc = db.parse(ff);
				if(doc.hasChildNodes())
				{
					Element root = doc.getDocumentElement();
					
					//to fix issue with ghost elements
					root.normalize();
					System.out.println(
							getNodeValue(
									getNode("java.vm.version", root.getChildNodes())
									)
							);
				}
				
			}
			catch (ParserConfigurationException e1)
			{
				//if error parsing xml
				e1.printStackTrace();
			}
			catch (SAXException e1)
			{
				//if erro with doc builder
				e1.printStackTrace();
			}
			catch (IOException e1)
			{
				e1.printStackTrace();
			}
		}

	}
	
	/**
	 * =================================================
	 * Get an XML file and create it if it doesn't exist
	 * @return A file object
	 * =================================================
	 */
	public static File getStateXMLFile()
	{
		Properties p = System.getProperties();
		File ff = new File(p.getProperty("user.dir")
				+"/src/java-props.xml");
		
		if(!ff.exists())
		{
			GetProperties.generateXML();
			ff = getStateXMLFile();
		}
		
		return ff;
	}
	
	/**
	 * =================================================
	 * Get a node from a node list
	 * @param tag
	 * @param nodes
	 * @return A Node object
	 * =================================================
	 */
	public static  Node getNode(String tag,NodeList nodes)
	{
		if(tag != null)
		{
			for(int x=0;x<nodes.getLength();x++)
			{
				Node n = nodes.item(x);
				if(n.getNodeName().equals(tag))
					return n;
			}
		}
		return null;
	}
	
	/**
	 * =================================================
	 * Get the node value
	 * @param n
	 * @return A String value
	 * =================================================
	 */
	public static String getNodeValue(Node n)
	{
		if(n != null)
		{
			NodeList childNodes = n.getChildNodes();
			for(int x = 0; x < childNodes.getLength(); x++)
			{
				Node data = childNodes.item(x);
				//we are checking if its a text node cuz we dont
				//want and empty node "GHOST"
				if(data.getNodeType() == n.TEXT_NODE)
					return data.getNodeValue();
			}
		}
		return null;
	}
	
	/**
	 * =================================================
	 * Get the attributes from a node
	 * @param attrName
	 * @param n
	 * @return a String value
	 * =================================================
	 */
	public static String getNodeAttr(String attrName,Node n)
	{
		/*
		 * when you have something like:
		 * <job company="sun" salary="25,000" location="san jose">
		 * 	a good job
		 * </job>
		 * 
		 * THis is a valid xml tag, job is tag and has bunch of attrs
		 * NamedNodeMap is a dictionary of attributes, and no
		 * ghost attributes
		 * 
		 * */
		NamedNodeMap attrs = n.getAttributes();
		for(int y = 0; y < attrs.getLength();y++)
		{
			Node attr = attrs.item(y);
			if(attr.getNodeName().equals(attrName))
				return attr.getNodeValue();
		}
		return null;
	}

}
