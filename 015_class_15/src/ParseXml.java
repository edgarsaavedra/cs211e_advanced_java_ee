import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.sun.xml.internal.txw2.Document;

public class ParseXml
{

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			Properties p = System.getProperties();
			Enumeration e = p.propertyNames();
			if(e.hasMoreElements())
			{
				DocumentBuilder db = dbf.newDocumentBuilder();
				File ff = new File(p.getProperty("user.dir")+"/src/java-props.xml");
				org.w3c.dom.Document doc = db.parse(ff);
				org.w3c.dom.Element root = doc.getDocumentElement();
				String root_name = root.getTagName();
				NodeList children = root.getChildNodes();
				//root name out put
				System.out.println("<"+root_name+">");
				
				root.normalize();
				
				//loop over children
				for(int i = 0; i < children.getLength();i++)
				{
					
					Node child = children.item(i);
					//deal with ghost elements 1
//					if(child instanceof org.w3c.dom.Element)
//					{
//						System.out.println("\t"+child.getNodeName());
//					}
					
					if(child.getNodeType() == Node.ELEMENT_NODE)
					{
						Element ee = (Element)child;
						Text t = (Text)ee.getFirstChild();
						System.out.print("\t<"+child.getNodeName());
						
						NamedNodeMap attrs = ee.getAttributes();
						
						if(attrs.getLength() > 0)
						{
							for(int ii = 0; ii < attrs.getLength(); ii++)
							{
								
								System.out.print(" "+attrs.item(ii).getNodeName()+"=\""+attrs.item(ii).getNodeValue()+"\" ");
							}
						}
						
						System.out.println(">");
						System.out.println("\t\t"+t.getData().trim());
						System.out.println("\t</"+child.getNodeName()+">");
					}
				}
				System.out.println("</"+root_name+">");
			}
		}
		catch (ParserConfigurationException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SAXException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		catch (IOException e1)
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

}
