<!--  
 Author: Edgar Saavedra
 Date  : 4/25/16
 file  : process-choices.jsp
 Objective: This file serves as a jsp file to 
 process get paramters and display system properties.
-->
<%@page import="java.util.Properties"%>
<%@page import="com.sun.javafx.runtime.SystemProperties"%>
<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>System Properties</title>
</head>
<body>
<table>
	<%
		Properties props = System.getProperties();
		//props.list(System.out);
	 
	
	 Enumeration requestNames = request.getParameterNames();
	%>
	<% if(requestNames.hasMoreElements()) { %>
	 <tr>
	  <td><h1>Your System Properties:</h1></td>
	 </tr>
 <% } else {%> 
	 <jsp:forward page="/">
   <jsp:param value="" name="nodata"/>
	 </jsp:forward>
	<% } %>
	<% 
	 while(requestNames.hasMoreElements())
	 {
	  String currentName = (String)requestNames.nextElement();
	  String currentValue = request.getParameter(currentName);
	%>
	 <tr>
	  <td>
			 <% if(currentName.equals("javaVersion")) { %>
			  <% if(currentValue.equals("on")) { %>
			   <b>Java Version:</b> <%= props.getProperty("java.version") %>
      <hr>
			  <% } %>
			 <% } %> <%-- End Enum Name --%>
			 
			 
			 <% if(currentName.equals("javaHome")) { %>
			  <% if(currentValue.equals("on")) { %>
			   <b>Java Home:</b> <%= props.getProperty("java.home") %>
      <hr>
			  <% } %>
			 <% } %> <%-- End Enum Name --%>
			 
			 
			 <% if(currentName.equals("osName")) { %>
			  <% if(currentValue.equals("on")) { %>
	     <b>OS Name:</b> <%= props.getProperty("os.name") %>
	     <hr>
			  <% } %>
			 <% } %> <%-- End Enum Name --%>
			 
			 
			 <% if(currentName.equals("uName")) { %>
			  <% if(currentValue.equals("on")) { %>
	     <b>User Name:</b> <%= props.getProperty("user.name") %>
      <hr>
			  <% } %>
			 <% } %> <%-- End Enum Name --%>
			 
			 
			 <% if(currentName.equals("homeDir")) { %>
			  <% if(currentValue.equals("on")) { %>
	     <b>Home Directory:</b> <%= props.getProperty("user.home") %>
      <hr>
			  <% } %>
			 <% } %> <%-- End Enum Name --%>
			 
			 
			 <% if(currentName.equals("curDir")) { %>
			  <% if(currentValue.equals("on")) { %>
	     <b>Current Directory:</b> <%= props.getProperty("user.dir") %>
      <hr>
			  <% } %>
			 <% } %> <%-- End Enum Name --%>
	  </td>
	 </tr>
	 
	<% }  
	 %>
 <tr>
  <td><a href="/Homework5/">Go Back To Form</a></td>
 </tr>
</table>
  
</body>
</html>