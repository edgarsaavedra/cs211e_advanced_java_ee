import java.sql.*;
public class Employee {

//	static boolean debug_mode = (System.getProperty("DEBUG")!=null);
	static boolean debug_mode = true;
	//in old java we had to register the driver
	// there is no need to do that >java 5
	// in the jar file the driver connection is done
	
	public static void main(String[] args) {
		//1 create connectivity, 
		// there is a static factory method in charge of creating the connection
		// when you want to connect to ay databse you need to have:
		// a) you need the connection url
		// b) userid
		// c) password
		
		// made by oracle=  jdbc:oracle:thin
		// jdbc:oracle:thin , this is called the protocol
		// the host: @hills.ccsf.edu
		// port number (oracle port number): 1521
				//oracle has 2 ports 1521 (unix,linux,macosx) & 1523 (microsoft)
		// Connectivity url: jdbc:oracle:thin:@hills.ccsf.edu:1521:cs12 
		// protocol:hosts:port:databasename
		try {
			// theese are abba's info

			/**
			 * ========================================
			 * create a connection
			 * ========================================
			 */
			//connector for oracle
//			Connection con = DriverManager.getConnection("jdbc:oracle:thin:@hills.ccsf.edu:1521:cs12","esaaved2","");
			//connector for mysql
//			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/cs211e","esaaved2","");
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			//localhost
			String userName = "root";
            String password = "root";
            String url = "jdbc:mysql://localhost:3306/cs211e";

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection con = DriverManager.getConnection(url, userName, password);
			if(con != null)
			{
				debug("connected to the database");
				Statement stmt = con.createStatement();
				
				/**
				 * ========================================
				 * Create table with field
				 * ========================================
				 */
//				//the execute method is allowing you to create any activity with no output
				String sql = "create table Greetings (Name varchar(20))";//sql command
				stmt.execute(sql);
				debug("Tabel created");
				

				/**
				 * ========================================
				 * Add data to field
				 * ========================================
				 */
				String sqlinsert = "insert into Greetings values ('bye,world!')";
				stmt.execute(sqlinsert);
				debug("done putting data");
				

				/**
				 * ========================================
				 * Query database
				 * ========================================
				 */
				//; is option in sql instructions
				String sqlquery = "select * from Greetings";
				

				/**
				 * ========================================
				 * Looping over data
				 * ========================================
				 */
				//we will need to loop over this object
				ResultSet result = stmt.executeQuery(sqlquery);
				//set the pointer to the next availabel record
				result.next();
				
				System.out.println(result.getString(1));
				//this is the same:
				// you can use field index or field name
//				System.out.println(result.getString("Name"));
				

				/**
				 * ========================================
				 * Drop table
				 * ========================================
				 */
				String droptable = "drop table Greetings";
//				stmt.execute(droptable);
				debug("table dropped");

				/**
				 * ========================================
				 * Close connection with query, sql statement & connection
				 * ========================================
				 */
				result.close();
				stmt.close();
				con.close();
			}
			
			
			
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}

	public static void debug(Object o) 
	{
		if(debug_mode)
		{
			System.out.println(""+o);
		}
	}

}
