import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletEx extends HttpServlet
{
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
	{
		try
		{
			String[] arr = {"test",
					"test2",
					"test3",
					"test4"};
			response.setContentType("text/html");
			PrintWriter printWriter = response.getWriter();
			printWriter.println("<h2>");
			printWriter.println(getInitParameter("email"));
			printWriter.println("</h2>");
			printWriter.println("<ul>");
			for(String str : arr)
				printWriter.println("<li>"+str+"</li>");
			printWriter.println("</ul>");
		}catch (IOException ioException)
		{
			ioException.printStackTrace();
		}
	}
	public ServletEx()
	{
		// TODO Auto-generated constructor stub
	}

}
