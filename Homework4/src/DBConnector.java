/*
 Author: Edgar Saavedra
 Date  : 4/19/16
 Program Name: DBConnector.java
 Homework: 4
 Objective: This program serves 
 as an API to connect to Oracle/SQL
*/
 import java.sql.*;
import java.util.*;

public class DBConnector 
{ 	
  static boolean debug_mode = (System.getProperties()
  		.getProperty("debug") != null);
  String connectionType = "";
  //****************************debug()*****************************
  public void debug(Object o) 
  {
    if(debug_mode)
    {
      System.out.println(""+o);
    }
  }
  //****************************DBConnector()************************
  public DBConnector(String type)
  {
     connectionType = type;
  }
  //****************************startCon()***************************
  public Connection startCon()
  {
    String userName = "";
    String password = "";
    String url = "";
 		
    //connector for oracle
    if(connectionType.equals("oracle"))
    {
      userName = "esaaved2";  
      password = "aug2287.es";
      url = "jdbc:oracle:thin:@hills.ccsf.edu:1521:cs12";
 		}
		//connector for mysql
 		else if ((connectionType.equals("mysql"))) 
 		{
 			userName = "root";
 			password = "root";
 			url = "jdbc:mysql://localhost:3306/cs211e";
 		}
 		try 
 		{
 			if(connectionType.equals("mysql"))
 			{
 				Class.forName("com.mysql.jdbc.Driver").newInstance();
 			}
 			Connection con = DriverManager
 					.getConnection(url, userName, password);
 			if(con != null)
 			{
 				debug("connected to the database");
 				return con;
 			}
 		} catch (SQLException e) 
 		{
 			e.printStackTrace();
 		} catch (InstantiationException e) 
 		{
 			e.printStackTrace();
 		} catch (IllegalAccessException e) 
 		{
 			e.printStackTrace();
 		} catch (ClassNotFoundException e)
 		{
 			e.printStackTrace();
 		}
 		return null;
 	}
	 //****************************createTable()***********************
 	public boolean createTable(Connection con)
 	{
 		if(con == null)
 			con = startCon();
 		try 
 		{
	 			debug("Creating tables...");
	 			Statement stmt = con.createStatement();
	 			
	 			String mysql_states = "CREATE TABLE States ("+
	 				"id INT NOT NULL,"+
	 				"state VARCHAR (250) NOT NULL,"+
	 				"capital VARCHAR (250) NOT NULL"+
					");";
				String oracle_states = "create table States ("+
					"id NUMBER(38) NOT NULL,"+
					"state VARCHAR2(20) NOT NULL,"+
					"capital VARCHAR2(20) NOT NULL"+
								")";
				if(connectionType.equals("mysql"))
					stmt.execute(mysql_states);

				if(connectionType.equals("oracle"))
					stmt.execute(oracle_states);
				stmt.close();
				debug("success: table created.");
				return true;
		} catch (SQLException e) 
		{
			debug("Couldn't create tables make sure tables do not exists.");
			e.printStackTrace();
			return false;
		}

	}
	 //****************************populateTables()********************
	public boolean populateTables(ArrayList<String> states,
			ArrayList<String>capitals,Connection con)
	{
		try 
		{
			debug("inserting states..");
			String sqlinsertStates = "insert into States "+
							"(id,state,capital) "+
	            "values (?,?,?)";
			PreparedStatement psStates = (PreparedStatement) con
					.prepareStatement(sqlinsertStates);
			for(int i = 1; i < states.size()+1; i++)
			{
				psStates.setInt(1, i);
				psStates.setString(2, states.get(i-1));
				psStates.setString(3, capitals.get(i-1));
				psStates.execute();
			}
			debug("success: states inserted.");
			return true;
		} catch (SQLException e) 
		{
			e.printStackTrace();
			debug("error inserting states & capitals.");
			return false;
		}
	}
	 //****************************dropTables()************************
	public boolean dropTables(Connection con,String tableName)
	{
		try 
		{
			String sql = "DROP TABLE "+tableName;
			Statement stmt;
			stmt = con.createStatement();
			stmt.execute(sql);
			debug("dropped table");
			return true;
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return false;
	}
	 //****************************getValueRegex()*********************
	public ResultSet getValueRegex(Connection con,String tableName,
			String colNameResult, String colName, String colVal)
	{
		try 
		{	
			String sql = "";
			if(connectionType.equals("oracle"))
				sql = "select "+colNameResult+" FROM "+tableName+" where "+
						"REGEXP_LIKE("+colName+", '"+colVal+"' )";
			if(connectionType.equals("mysql"))
				sql = "select "+colNameResult+" FROM "+tableName+" where "+
						colName+" "+"REGEXP"+" '"+colVal+"'";
			Statement stmt;
			stmt = con.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			return result;
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}
	
		 //****************************getValueWhere()*******************
	public String getValueWhere(Connection con,String tableName,
			String colNameResult, String colName, String colVal)
	{
		try 
		{
			String sql = "select "+colNameResult+" FROM "+tableName+
					" where "+colName+"='"+colVal+"'";
			Statement stmt;
			stmt = con.createStatement();
			ResultSet result = stmt.executeQuery(sql);
			if(result.next())
				return result.getString(1);
		} catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return null;
	}

}
