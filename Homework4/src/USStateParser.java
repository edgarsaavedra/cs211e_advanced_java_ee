/*
 Author: Edgar Saavedra
 Date  : 4/19/16
 Program Name: USStateParser.java
 Homework: 4
 Objective: This program serves 
 as parser to parse US states files.
*/
import java.io.*;
import java.util.*;

public class USStateParser
{
		private String fileName;
	 //****************************USStateParser()*********************
		public USStateParser(String filePath)
		{
			fileName = filePath;
		}
		
	 //****************************parseUSStates()*********************
		private void parseUSStates(ArrayList<String> states, 
				ArrayList<String> capitals)
		{
			try
			{
				Scanner sc = new Scanner(new File(fileName));
				String line;
				int i = 0;
				
				sc.nextLine(); sc.nextLine();

				while(sc.hasNext())
				{
					line = sc.nextLine();
					String temp[] = line.split("\\s\\s+");
					if(temp.length >= 2)
					{
						if(temp.length == 2) 
						{
							states.add(temp[0]);
							capitals.add(temp[1]);
						}
						else
						{
							states.add(temp[0] + " " + temp[1]);
							capitals.add(temp[2]);
						}
					}
				}
				
			}catch(FileNotFoundException e){System.err.println(e);}
		}

	 //****************************parseUSStatesArr()******************
		public Map parseUSStatesArr()
		{
			Map collection = new TreeMap<>();
			ArrayList<String> states   = new ArrayList<>();
			ArrayList<String> capitals = new ArrayList<>();

			parseUSStates(states, capitals);
			
			collection.put("states", states);
			collection.put("capitals", capitals);
			
			return collection;
		}
}
