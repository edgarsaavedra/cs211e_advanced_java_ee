/*
 Author: Edgar Saavedra
 Date  : 4/19/16
 Program Name: PostServlet.java
 Homework: 4
 Objective: This program serves 
 as a servlet to process post request for our
 USState application.
*/
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class PostServlet extends HttpServlet	
{
	private Connection con;
	private DBConnector connection;		
	private USStateParser parser;
	private	ArrayList<String> states;
	private	ArrayList<String> capitals;
  //**************************createTable()**************************
	public void createTable(Connection con)
	{
    connection.createTable(con);
	}
  //************************populateTable()**************************
	public void populateTable(Connection con)
	{
		String filepath = "/Users/edgar/Google Drive/CCSF-Edgar/2016/"+
      "spring/cs211e_advanced_java_ee/Homework2/src/US_states";
    parser = new USStateParser(filepath);
    if(parser != null)
    {
      Map collection = parser.parseUSStatesArr();
      collection = parser.parseUSStatesArr();
      capitals = (ArrayList<String>) collection.get("capitals");
      states   = (ArrayList<String>) collection.get("states");
  		//Handle our wanted connections
	    connection.populateTables(states, capitals, con);
    }
	}
  //***************************dropTable()**************************
	public void dropTable(DBConnector connection,Connection con)
	{
    connection.dropTables(con, "States");
	}
  //****************************getState()**************************
	public String getState(String capitalRegex)
	{
		 String state = connection.getValueWhere(con,"States",
					"state", "capital",capitalRegex);
			return (state != null)?state:null;
	}
	 //****************************getCapital()************************
	public String getCapital(String stateRegex)
	{
		 String cap = connection.getValueWhere(con,"States",
					"capital", "state",stateRegex);
			return (cap != null)?cap:null;
	}
	//****************************closeConnection()*******************
	public void closeConnection()
	{
		if(con != null)
	    try
     {
	      con.close();
     } catch (SQLException e)
     {
	      e.printStackTrace();
     }
	}
	//****************************doGet()*******************
	public void doGet(HttpServletRequest req, HttpServletResponse res) 
			throws ServletException, IOException
	{
		doPost(req,res);
	}
	//****************************doPost()*******************
	public void doPost(HttpServletRequest req, HttpServletResponse res)
	    throws ServletException, IOException
	{
		//if no params
		if(req.getParameter("stateOrCapital") == null 
				|| req.getParameter("query") == null)
		{
			res.setHeader("Location", "/Homework4");
			res.setStatus( HttpServletResponse.SC_TEMPORARY_REDIRECT);
		}
		//if empty params
		else if(req.getParameter("stateOrCapital").equals("") 
				|| req.getParameter("query").equals(""))
		{
			res.setHeader("Location", "/Homework4");
			res.setStatus( HttpServletResponse.SC_TEMPORARY_REDIRECT);
		}
		else
		{
			res.setContentType("text/html");
			PrintWriter pw = res.getWriter();
			pw.println("<head><title>Ger Order Served</title></head>");
			pw.println("<body>");
			pw.println("<link rel='stylesheet'"+
				"href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/"+
				"css/bootstrap.min.css' >");
      connection = new DBConnector("mysql");
      if(connection != null)
      {
        //start a connection
        con = connection.startCon();
        
        //create tables if needed here
	      //createTable(con);
	      //populateTable(con);
        //see if we are searching state or capital
    		String result = req.getParameter("stateOrCapital");
  			pw.println("<div class='container'>");
	  			if(result.equals("state"))
	  			{
	      		result = req.getParameter("query");
	    			String str = getCapital(result);
	      		if(str != null)
	      		{
	      			pw.println("<h1>Success!</h1>");
	      			pw.println("<h2>State: <em>"+result+"</em></h2>");
	        		pw.println("<h2>Has Capital: <em>"+str+"</em></h2>");
	        		pw.println("<p><a href='/Homework4'>"+
	        		"Search More</a></p>");
	      		}else
	      		{
	      			pw.println("<h1>There is no data for the state:"+
	      					" "+result+"</h1>");
	        		pw.println("<p>Please be sure the state exists</p>");
	        		pw.println("<p><a href='/Homework4'>"+
	        		"Search More</a></p>");
	      		}
	  			}else
	  			{
	      		result = req.getParameter("query");
	    			String str = getState(result);
	    			if(str != null)
	    			{
	      			pw.println("<h1>Success!</h1>");
	      			pw.println("<h2>Capital: <em>"+result+"</em></h2>");
	        		pw.println("<h2>Has State: <em>"+str+"</em></h2>");
	        		pw.println("<p><a href='/Homework4'>"+
	        		"Search More</a></p>");
	    			}else
	    			{
	      			pw.println("<h1>There is no data for the capital:"+
	      					" "+result+"</h1>");
	        		pw.println("<p>Please be sure the "+
	      					"capital exists</p>");
	        		pw.println("<p><a href='/Homework4'>"+
	        		"Search More</a></p>");
	    			}
	  			}
	  			pw.println("</div>");
      }
			pw.println("</body>");
			//close db connection
			closeConnection();
			pw.close();
		}
	}
}