import java.util.Random;

/**
 * If you want to have predefined 
 * internface implementations
 * @author edgar
 *
 */
public interface MyLib
{
	static int getDefalutNumber() { return 0;}
	int getNumber();
	default String getString()
	{
		return "default valie";
	}
	default Integer rand(Integer a, Integer b)
	{
		return (new Random().nextInt((b - a)+1)+a);
	}
	default String $0()
	{
		return (Thread.currentThread().getStackTrace()[1].getClassName()+".java");
	}
}
