/**
 * There can only be one method that 
 * has no implementation and is abstract
 * this is often used in Lambada Expressions
 * 
 * We had many functiona interfaces ie ActionListener for button
 * It was renamed to "FUNCTIONAL INTERFACE"
 * @author edgar
 *
 */
public interface FunctionalInterface
{

}
