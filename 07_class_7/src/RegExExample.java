import java.util.regex.*;
/**
 * You normally need only 2 classes to use
 * regular expressions Pattern & Matcher
 * 
 * We use the PatterClass to define our regular
 * expressions 
 * 
 * We use Matcher that suggest group of methods
 * to use to take care RegEx. Pattern class 
 * has no constructor
 * @author edgar
 *
 */
public class RegExExample
{

	public RegExExample()
	{
	}
	
	/**
	 * This will be on the mid term!!!!!!!!!!!!!!
	 * @param str
	 * @param regex
	 * @return
	 */
	public static boolean regex(String str,String regex)
	{
		Pattern pat;
		Matcher mat;
		pat = Pattern.compile(regex);
		mat = pat.matcher(str);
		return mat.find();
	}
	
	public static void main(String[] args)
	{
		Pattern pat;
		Matcher mat;
		//anything begins with "Java" upper or lower
		pat = Pattern.compile("^[Jj]ava");
		mat = pat.matcher("Java is great");
		//find is one of the methods we use to findout if match is taking place or not
		if(mat.find())
			System.out.println("it has java");
		else
			System.out.println("it does not have java");
		
		pat = Pattern.compile("hot");
		mat = pat.matcher("Today was hot and I had a cup of hot tea");
		while(mat.find())
			System.out.println("hot was found at index "+mat.start());
		
		System.out.println(regex("I like java","[Jj]ava"));
		
		/*
		 * This willl not work
		 */
		String str = "I like java";
		System.out.println(str.matches("[Jj]ava"));
		
		/*
		 * this will work
		 */
		// begin with 0 or more characters followed by java at the end
		System.out.println(str.matches("^.*[Jj]ava$"));
	}

}
