import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;


public class RegisteryServer extends UnicastRemoteObject 
{

	public RegisteryServer() throws RemoteException
	{
		super();
	}

  public double add(double a, double b) throws RemoteException {
	return (a+b);
  }
    
  public static void main(String[] args) 
  {
	  if(System.getSecurityManager() == null)
	  {
		  System.setSecurityManager(null);
	  }
	  try
	  {
	  		RegisteryServer asi = new RegisteryServer();
         //create a server
         //Rebind is placing our methods into
         //registery, this is saying that all methods
         //in the interface should be registered
         LocateRegistry.createRegistry(6600);
         Naming.rebind("rmi://127.0.0.1:6600/Addserver", asi);
//         System.out.println(LocateRegistry.getRegistry().list());
         System.out.println("AddServer Ready");
	  }catch(RemoteException e){
		  e.printStackTrace();
	  } 
	  catch (MalformedURLException e) {
		e.printStackTrace();
	  }
	  
  }
}
