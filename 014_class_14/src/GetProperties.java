import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.Enumeration;
import java.util.Properties;

public class GetProperties
{
	//user.dir
	public static void main(String[] args)
	{
		Properties p = System.getProperties();
		Enumeration e = p.propertyNames();
		if(e.hasMoreElements())
		{
			try
			{
				//DTD root element string
				String str = "";
				PrintWriter pw = new PrintWriter(p.getProperty("user.dir")+"/src/java-props.xml","UTF-8");
				
				//XML header
				pw.println("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
				
				//the DTD root element
				pw.println("<!DOCTYPE java[");

				pw.print("<!ELEMENT java  (");
				while(e.hasMoreElements())
				{
					String propName = e.nextElement().toString();
					if(propName.indexOf("java") > -1)
					{
						if(e.hasMoreElements())
						{
							 str = str + propName+",";
						}
					}
				}
				
				pw.print(str.substring(0, (str.length()-1)));
				pw.print(")>\n");
				
				//the DTD elements
				e = p.propertyNames();
				while(e.hasMoreElements())
				{
					String propName = e.nextElement().toString();
					if(propName.indexOf("java") > -1)
					{
						pw.print("<!ELEMENT "+propName+"  (#PCDATA)>\n");
					}
				}
				pw.println("]>");
				
				//the XML data
				pw.println("<java>");
				e = p.propertyNames();
				while(e.hasMoreElements())
				{
					String propName = e.nextElement().toString();
					if(propName.indexOf("java") > -1)
					{
						pw.print("<"+propName+">");
						pw.print(p.getProperty(propName));
						pw.print("</"+propName+">\n");
					}
				}
				pw.println("</java>");
				
				pw.close();
			}
			catch (FileNotFoundException e1)
			{
				e1.printStackTrace();
			}
			catch (UnsupportedEncodingException e1)
			{
				e1.printStackTrace();
			}
		}
	}

}
