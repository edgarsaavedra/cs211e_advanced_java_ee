/*
 
 Author: Abbas Moghtanei 
 Date  : 04/10/11
 Program Name: DemoAMYaml.java
 
*/
 
import java.util.*;
import java.io.*;
 
class DemoAMYaml
{
   public static void main(String args[])
   {
      AMYaml amy = new AMYaml();
      HashMap<String, String> hm = new HashMap<String, String>();

      hm.put("name", "John");
      hm.put("age", "30");
      hm.put("sex", "M");
      hm.put("job", "Engineer");
      hm.put("phone", "(415)928-5678");

      amy.dump(hm, "AMYaml.conf");

      HashMap<String, String> hm1 = amy.load("AMYaml.conf");
      Set<Map.Entry<String,String>> set = hm1.entrySet();

      for(Map.Entry<String,String> me : set)
      {
         System.out.print(me.getKey() + ": ");
         System.out.println(me.getValue());
      }
      
   }
}
