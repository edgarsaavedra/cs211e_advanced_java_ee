#include <stdio.h>
#include <stdlib.h>
#include <termio.h>
/*****************************getch()*************************/
char getch(void)
{
   struct termio save, term;
   char c;

   if(ioctl(0, TCGETA, &term) == -1) die("ioctl");
   save = term;  /* to set back the terminal to default */

   
   term.c_lflag &= ~ECHO;    /* no echo mode      */
   term.c_lflag &= ~ICANON;  /* no canonical mode */
   term.c_cc[VMIN]  = 1;     /* one byte raw mode */
   term.c_cc[VTIME] = 0;     /* wait indefinitely until a char is entered */

   if(ioctl(0, TCSETA, &term) == -1) die("ioctl");
   if(read(0, &c, 1) == -1) die("read");
   
   if(ioctl(0, TCSETA, &save) == -1) die("ioctl");
   return(c);
}
/*****************************getche()*************************/
char getche(void)
{
   struct termio save, term;
   char c;

   if(ioctl(0, TCGETA, &term) == -1) die("ioctl");
   save = term;  /* to set back the terminal to default */

   term.c_lflag &= ~ICANON;  /* no canonical mode */
   term.c_cc[VMIN]  = 1;     /* one byte raw mode */
   term.c_cc[VTIME] = 0;     /* wait indefinitely until a char is entered */

   if(ioctl(0, TCSETA, &term) == -1) die("ioctl");
   if(read(0, &c, 1) == -1) die("read");
   
   if(ioctl(0, TCSETA, &save) == -1) die("ioctl");
   return(c);
}

