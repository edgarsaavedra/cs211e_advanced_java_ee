// Author: Abbas Moghtanei // Date  : 11/18/12
// This is the ij library used in the ij program

package ijpackage;

  import java.util.*;
  import java.util.regex.*;
  import java.net.*;
  import java.io.*;
  import javax.swing.*;
  import java.applet.*;
  import java.awt.*;
  import java.awt.event.*;
  import java.lang.reflect.*;
  import java.sql.*;
  


public class Libij 
{
   public static void println(Object o){System.out.println(""+o);}
   public static void print(Object o){System.out.print(""+o);}
   public static void printf(String s,Object ... o){System.out.printf(s,o);}
   public static void p(Object o){System.out.println(""+o);}
   public static void pf(String s,Object ... o){System.out.printf(s,o);}
   public static void p(Object a[]){for(Object o:a) p(o);}
   public static void p(double a[]){for(double d:a) p(d);}
   public static void p(int a[]){for(int i:a) p(i);}
   public static void p(long a[]){for(long l:a) p(l);}
   public static void p(byte a[]){for(byte b:a) p(b);}
   public static void p(boolean b[]){for(boolean x:b) p(x);}
   public static void p(){p("");}
   public static void puts(Object o){System.out.println(""+o);}
   public static void die(String msg){System.err.println(msg);System.exit(1);}
   
   public static int  ord(char c){return((int)c);}
   /******************************count()****************************/
   public static int count(String s, String t)
   {   int cnt = 0, lt = t.length(), x=0;
       if(lt == 0) return(0);
       while((x = s.indexOf(t,x)) != -1)
       { cnt++; x += lt; }
       return(cnt);
   }
   public static char chr(int n){return((char)n);}
   public static double square(double num){return(Math.pow(num,2.0));}
   public static String left(String s,int len){return s.substring(0,len);}
   public static String right(String s,int len)
   {return s.substring(s.length()-len);}

   /******************************grep()*****************************/
   public static String[] grep(String a[], String regex)
   {   ArrayList<String> al = new ArrayList<String>();
       for(String s : a) if (s.matches(regex)) al.add(s);
       String ar[]  = new String[al.size()];
       al.toArray(ar);
       return(ar);
   }
   /*********************************$()*****************************/
   public static String[] $(String str)
   {
       if(str.isEmpty()) throw new IllegalArgumentException("empty string");
       return(str.split("\\s"));
   }
   /****************************capitalize()*************************/
   public static String capitalize(String str)
   {
       if(str.isEmpty()) return(str);
       String sum = "";
       char c;
       boolean wasSpaceBefore = true;
       str = str.toLowerCase();
 
       for(int i = 0; i < str.length(); i++)
       {
          c = str.charAt(i);
          if(Character.isWhitespace(c)) wasSpaceBefore = true;
          else if(Character.isLowerCase(c) && wasSpaceBefore)
          {
             c = Character.toUpperCase(c);
             wasSpaceBefore = false;
          }
          sum += c;
       }
       return(sum);
   }
   /******************************rtrim()****************************/
   public static String rtrim(String s)
   {
       if(s.isEmpty()) throw new IllegalArgumentException("empty string");
       int i;
       for(i = s.length()-1;i >=0; i--)
       {
          if(!Character.isWhitespace(s.charAt(i))) break;
       }
       return(s.substring(0,i+1));
   }
   /******************************ltrim()****************************/
   public static String ltrim(String s)
   {
       if(s.isEmpty()) throw new IllegalArgumentException("empty string");
       int i;
       for(i = 0; i < s.length(); i++)
       {
          if(!Character.isWhitespace(s.charAt(i))) break;
       }
       return(s.substring(i));
   }
//********************************strip()*********************************
   public static String strip(String s)
   {
      if(s.isEmpty()) return(s);

      s = s.trim();

      char c;
      String str = "";
      int len = s.length();

      for(int i = 0; i < len; i++)
      {
         c = s.charAt(i);
         if (!Character.isWhitespace(c))  str += c;
      }
      return(str);
   }   
   /******************************join()*****************************/
   public static String join(String s[], String d)
   {
       String str = "";
       for(String t : s) str += t+d;
       return(str.substring(0,str.length()-d.length()));
   }


   /******************************rand()*****************************/
   public static int rand(int a, int b)
   {
       return((int)((b-a+1)*Math.random() + a));
   }
   /******************************sleep()****************************/
   public static void sleep(long ... time)
   {
       try
       {
            switch(time.length)
            {
                case 0 : Thread.sleep(100); break;
                case 1 : Thread.sleep(time[0]); break;
                default: Thread.sleep(time[0], (int)time[1]); break;
            }
       }
       catch(InterruptedException e){}
   }
//*******************************isPrime()********************************
   public static boolean isPrime(int num)
   {
      for(int i = 2; i <= Math.sqrt(num); i++)
      {
         if(num % i == 0) return(false);
      }
      return(true);
   }
//******************************getPrimes()*******************************
   public static int[] getPrimes(int from, int to)
   {

      java.util.List<Integer> list = new ArrayList<Integer>();
      int i = 0;

      for(int n = from; n <= to; n++)
      {
          if(isPrime(n)) list.add(n);
      }

      int buf[] = new int[list.size()];
      for(Integer n : list) buf[i++] = n;
      return(buf);
   } 
//*****************************toArray()******************************
   public static int[] toArray(ArrayList<Integer> al)
   {
      int buf[] = new int[al.size()];
      int i = 0;

      for(Integer n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static double[] toArray(ArrayList<Double> al, String ... ss)
   {
      double buf[] = new double[al.size()];
      int i = 0;

      for(Double n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static float[] toArray(ArrayList<Float> al, short ... ss)
   {
      float buf[] = new float[al.size()];
      int i = 0;

      for(Float n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static byte[] toArray(ArrayList<Byte> al, long ... kk)
   {
      byte buf[] = new byte[al.size()];
      int i = 0;

      for(Byte n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static String[] toArray(ArrayList<String> al, boolean ... bb)
   {
      String buf[] = new String[al.size()];
      int i = 0;

      for(String n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static long[] toArray(ArrayList<Long> al, char ... cc)
   {
      long buf[] = new long[al.size()];
      int i = 0;

      for(Long n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static char[] toArray(ArrayList<Character> al, float ... ff)
   {
      char buf[] = new char[al.size()];
      int i = 0;

      for(Character n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static short[] toArray(ArrayList<Short> al, byte ... bb)
   {
      short buf[] = new short[al.size()];
      int i = 0;

      for(Short n : al) buf[i++] = n;
      return(buf);
   }
//*****************************toArray()******************************
   public static boolean[] toArray(ArrayList<Boolean> al, int ... ii)
   {
      boolean buf[] = new boolean[al.size()];
      int i = 0;

      for(Boolean n : al) buf[i++] = n;
      return(buf);
   }
//*******************************urand()******************************
   public static int[] urand(int a, int b, int n)
   {
      if(a > b || a < 0 || b < 0 || n < 0) die("Invalid argument(s)");
      if(n > b - a + 1) die("Invalid range");
      
      HashSet<Integer> set = new HashSet<Integer>();
      while(set.size() < n) set.add(rand(a,b));

      int buf[] = new int[n];
      int j = 0;
      for(Integer i : set) buf[j++] = i;
      return(buf);
   } 
//*****************************system()********************************
/*   public static String[] system(String cmd)
   {
      if(cmd.length() == 0) die("Error: missing command");
      ArrayList<String> list = new ArrayList<String>();
      try
      {
          // cmd = "bash -c " + cmd;
          Process p = new 
            ProcessBuilder($(cmd)).redirectErrorStream(true).start();

          Scanner sc = new Scanner(p.getInputStream());

          while(sc.hasNext()) list.add(sc.nextLine());
      }catch(Exception e){ System.err.println(e); }
      return(toArray(list));
   }
*/
//*****************************system()********************************
   public static String[] system(String cmd)
   {
      if(cmd.length() == 0) die("Error: missing command");
      if(has(System.getProperty("os.name"), "Windows"))
         die("The system method can not be used for Windows OS"); 

      String shellPath = System.getenv("SHELL"); 
      String shell;
      if (shellPath == null) shell = "bash";
      else
      {
          File ff = new File(shellPath);
          shell   = ff.getName();
      }
          
      File f = new File("ij000." + shell);
      try
      {
         PrintWriter pw = new PrintWriter(f);
         pw.println("#!" + shellPath);
         pw.println(cmd);
         pw.println("exit 0");
         pw.close();
      }catch(IOException e){}
             
      try
      {
          Process p = new ProcessBuilder($("chmod 700 ij000." + shell)).
           redirectErrorStream(true).start();

      }catch(Exception e){ System.err.println(e); }
      //f.setExecutable(true); 
      ArrayList<String> list = new ArrayList<String>();
      try
      {
          // cmd = "bash -c " + cmd;
          Process p = new 
            ProcessBuilder("ij000." + shell).redirectErrorStream(true).start();

          Scanner sc = new Scanner(p.getInputStream());

          while(sc.hasNext()) list.add(sc.nextLine());
      }catch(Exception e){ System.err.println(e); }
      return(toArray(list));
   }
//*******************************has()*********************************
   public static boolean has(String s, String t, boolean ... ignoreCase )
   {
       if(ignoreCase.length == 0 || ignoreCase[0] == false)
       {
          return(s.indexOf(t) != -1);
       }
       else
       {
          String str = s.toUpperCase();
          String target = t.toUpperCase();
          return(str.indexOf(target) != -1);
       }
   }
//*******************************has()*********************************
   public static boolean has(String s, char c, boolean ... ignoreCase)
   {
       if(ignoreCase.length == 0 || ignoreCase[0] == false)
       {
          return(s.indexOf(c) != -1);
       }
       else
       {
          String str = s.toUpperCase();
          char   uc  = Character.toUpperCase(c);
          return(str.indexOf(uc) != -1);
       }
   }
//*****************************hasMethod()*****************************
   public static boolean hasMethod(String klass, String aMethod)
   {
       Class c = null;
       try
       {
          c = Class.forName(klass);
       }catch(ClassNotFoundException e){die("class not found");}

       Method meth[] = c.getDeclaredMethods();;
       for(Method m : meth)
       {
           if(m.getName().equals(aMethod)) return(true);
       }
       return(false);
   }
//*********************************chop()*********************************
   public static String chop(String str)
   {
      int len = str.length();
      return((len == 0) ? str : str.substring(0, len-1)); 
   }
//********************************chomp()*********************************
   public static String chomp(String ... str)
   {
      String delim = (str.length == 1) ? "\n" : str[1];
      String s = str[0];
      int len = s.length();
      int dlen = delim.length();

      if(dlen > len) return(s);
      return((right(s, dlen).equals(delim)) ? left(s, len - dlen) : s);
   }
   
//********************************reverse()*******************************
   public static String reverse(String s)
   {
       StringBuffer sb = new StringBuffer(s);
       return(new String(sb.reverse()));
   }
//********************************version()*******************************
   public static double version()
   {
      String v[] = System.getProperty("java.version").split("_");
      int n = v[0].lastIndexOf('.');
      String s = v[0].substring(0, n);
      return(Double.parseDouble(s) * 10.0 - 10.0 + 
             Double.parseDouble(v[1])/100.0);
   }
//******************************isMethodOf()******************************
   public static boolean isMethodOf(String klass, String m)
   {
      try
      {
          String xclass = klass;
          if(klass.indexOf('.') == -1)
             xclass = "java.lang." + klass;
          Class<?> c = Class.forName(xclass);
          Method[] methods = c.getDeclaredMethods();
          for(Method met : methods)
          {
             if(met.getName().equals(m)) return(true);
          }
          return(false);
      }catch(ClassNotFoundException e){}
      return(false);
   }
//********************************values()********************************
   public static int[] values(int arr[], int ... x)
   {
      ArrayList<Integer> al = new ArrayList<Integer>();

      if (x.length == 0) return(arr);
      int len = arr.length;

      for(int n : x)
      {
          if( Math.abs(n) >= len )
              throw new ArrayIndexOutOfBoundsException();
          if(n >= 0) al.add(arr[n]);
          else       al.add(arr[len + n]); 
      }
      return(toArray(al));
   }
//********************************values()********************************
   public static  String[] values(String arr[], int ... x)
   {
      ArrayList<String> al = new ArrayList<String>();

      if (x.length == 0) return(arr);
      int len = arr.length;

      for(int n : x)
      {
          if( Math.abs(n) >= len )
              throw new ArrayIndexOutOfBoundsException();
          if(n >= 0) al.add(arr[n]);
          else       al.add(arr[len + n]); 
      }
      return(toArray(al));
   }
//********************************values()********************************
   public static double[] values(double arr[], int ... x)
   {
      ArrayList<Double> al = new ArrayList<Double>();

      if (x.length == 0) return(arr);
      int len = arr.length;

      for(int n : x)
      {
          if( Math.abs(n) >= len )
              throw new ArrayIndexOutOfBoundsException();
          if(n >= 0) al.add(arr[n]);
          else       al.add(arr[len + n]); 
      }
      return(toArray(al));
   }
//********************************values()********************************
   public static char[] values(char arr[], int ... x)
   {
      ArrayList<Character> al = new ArrayList<Character>();

      if (x.length == 0) return(arr);
      int len = arr.length;

      for(int n : x)
      {
          if( Math.abs(n) >= len )
              throw new ArrayIndexOutOfBoundsException();
          if(n >= 0) al.add(arr[n]);
          else       al.add(arr[len + n]); 
      }
      return(toArray(al));
   }
//********************************values()********************************
   public static boolean[] values(boolean arr[], int ... x)
   {
      ArrayList<Boolean> al = new ArrayList<Boolean>();

      if (x.length == 0) return(arr);
      int len = arr.length;

      for(int n : x)
      {
          if( Math.abs(n) >= len )
              throw new ArrayIndexOutOfBoundsException();
          if(n >= 0) al.add(arr[n]);
          else       al.add(arr[len + n]); 
      }
      return(toArray(al));
   }
//*******************************fileSize()*******************************
   public static long fileSize(String fname)
   {
       if(fname.isEmpty()) throw new IllegalArgumentException("empty string");

       File f = new File(fname);
       return(f.length());
   }
//********************************matches()*******************************
   public static boolean matches(String ... str)
   {
      if(str.length < 2 || str.length > 3)
         throw new IllegalArgumentException("Invalid Arguments");

      String s = str[0], regexp = str[1];
      char option = ' ';
      Pattern pat;
      Matcher mat;

      if(str.length == 3) option = str[2].charAt(0);

      pat = (option == 'i') ? Pattern.compile(regexp,
                    Pattern.CASE_INSENSITIVE): Pattern.compile(regexp);
                    mat = pat.matcher(s);
                    return(mat.find());
   }
}
