import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class PostServlet extends GenericServlet	
{

	@Override
	public void service(ServletRequest req, ServletResponse res)
	    throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		
		//getParameterNames means lots of items
		Enumeration e = req.getParameterNames();
		
		//get the name and value of submitted fields
		while(e.hasMoreElements())
		{
			String pname = (String)e.nextElement();
			pw.print(pname+"=");
			String pvalue = req.getParameter(pname);
			pw.println(pvalue);
		}
		pw.close();
		
		
	}


}
