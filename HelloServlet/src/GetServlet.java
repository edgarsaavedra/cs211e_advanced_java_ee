import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GetServlet extends HttpServlet	
{

	public void doGet(HttpServletRequest req, HttpServletResponse res)
		throws ServletException,IOException
	{
		String color = req.getParameter("color");
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		pw.println("<b>The Selected Color: "+color+"</b>");
		pw.close();
	}

}
