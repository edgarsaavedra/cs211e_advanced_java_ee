import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;

public class HelloServlet extends GenericServlet
{
	@Override
	public void service(ServletRequest req, ServletResponse res)
	    throws ServletException, IOException
	{
		//set the MIME type
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		pw.println("<b>Hello!</b>");
		pw.close();
	}

}
