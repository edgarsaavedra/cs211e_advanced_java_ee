import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.plaf.basic.BasicTreeUI.SelectionModelPropertyChangeHandler;

public class MultiThreading extends JFrame
{

	public MultiThreading()
	{
		setTitle("tilte");
		setSize(100, 100);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(new Runnable()
		{
			
			@Override
			public void run()
			{
				new MultiThreading();
			}
		});
	}
}
