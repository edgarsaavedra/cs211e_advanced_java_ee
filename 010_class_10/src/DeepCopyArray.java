import java.lang.reflect.Array;
import java.util.Arrays;

public class DeepCopyArray
{

	/*
	 * Shallow copies duplicate as little as possible. 
	 * A shallow copy of a collection is a copy of the 
	 * collection structure, not the elements. With a shallow copy, 
	 * two collections now share the individual elements.
	 * 
	 * Deep copies duplicate everything. A deep copy of a collection 
	 * is two collections with all of the elements in the original
	 * collection duplicated.
	 * 
	 */
	public DeepCopyArray()
	{
		
	}
	public static void main(String[] args)
	{
		int[] intarr = {1,2,3,4,5};
		System.out.println(Arrays.toString(intarr));
		
		System.out.println("shallow copy");
		intarr = Arrays.copyOf(intarr, 3 * intarr.length);
		System.out.println(Arrays.toString(intarr));
		
		System.out.println("Deep copy");
		intarr = (int[])copyArray(intarr, 10);
		System.out.println(Arrays.toString(intarr));
		
	}
	public static Object copyArray(Object arr, int newlength)
	{
		/**
		 * To create object of C Class
		 * 1. we create an object of the array
		 * 2. array should be and array
		 */
		
		Class c = arr.getClass();
		
		// in industry example this is raising an exception
		if(!c.isArray())return null;
		
		//this is method that is returning an object of the array
		Class ct = c.getComponentType();
		//get the length
		int len = Array.getLength(arr);
		
		// creating a new instance,this is completly new instance
		Object newArray = Array.newInstance(ct, newlength);
		
		System.arraycopy(arr, 0, newArray, 0, Math.min(len, newlength));
		
		return newArray;
		
	}
}
