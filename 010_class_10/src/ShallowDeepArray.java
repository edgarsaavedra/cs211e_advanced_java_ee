import java.util.*;

public class ShallowDeepArray
{

	public ShallowDeepArray()
	{
		String[] a = new String[100];
		//this helps expand an array
		//this is a shallow copy
		// this would not satisfy  a shallow copy
		a = Arrays.copyOf(a, 2 * a.length);
	}

}
