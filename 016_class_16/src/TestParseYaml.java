import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class TestParseYaml
{
	public static void main(String[] args)
	{

		Properties p = System.getProperties();
		String filepath = p.getProperty("user.dir")+"/good.yml";
//		System.out.println(filepath);
		AMYaml amy = new AMYaml();
		HashMap<String, String> hm1 = amy.load(filepath);
		
		Set<Map.Entry<String,String>> set = hm1.entrySet();
		
		for(Map.Entry<String, String> me : set)
		{
			System.out.print(me.getKey()+" : ");
			System.out.println(me.getValue());
		}
	}
}
