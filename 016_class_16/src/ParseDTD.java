

import java.io.File;
import java.util.Arrays;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXParseException;

import com.sun.org.apache.xerces.internal.jaxp.validation.StAXValidatorHelper;

import jdk.internal.org.xml.sax.SAXException;

public class ParseDTD
{
	public static void main(String[] args)
	{
		System.out.println(Arrays.toString(args));
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try
		{
			boolean validate = args[1].equals("true")?true:false;
			
			//this line is very important, this means that we are validating
			//with the dtd file. In the xml we specify, which dtd file 
			//to use
			dbf.setValidating(validate);

			DocumentBuilder db = dbf.newDocumentBuilder();
//		DocumentBuilder doc = dbf.parser(new File(args[0]));
			org.w3c.dom.Document doc = db.parse(new File(args[0]));
//			DocumentBuilder doc = (DocumentBuilder) dbf.newDocumentBuilder().parse(new File(args[0]));
			Element root = ((Document) doc).getDocumentElement();
			root.normalize();
			
//			NodeList titles = root.getElementsByTagName("title");
			NodeList titles = root.getElementsByTagName("name");
			for(int i = 0; i < titles.getLength(); i++)
			{
//				Element e = (Element)titles.item(i);
//				String full = e.getAttribute("full");
//				Text text = (Text) e.getFirstChild();
//				String result = full.equals("false")?"is not":"is";
//				System.out.println(text.getNodeValue()+result+" the full title");
				Element e = (Element)titles.item(i);
				Text text = (Text) e.getFirstChild();
				System.out.println(text.getNodeValue());
			}
		}
		catch(SAXParseException e)
		{

			System.out.println("dtd has an error");
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
