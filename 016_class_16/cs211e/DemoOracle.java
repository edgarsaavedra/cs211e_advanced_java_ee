/*
 
 Author: Abbas Moghtanei 
 Date  : 01/29/12
 Program Name: DemoOracle.java
 
*/
 
import java.util.*;
import java.io.*;

class DemoOracle
{
   public static void main(String args[])
   {
     String sqlcmd="select distinct username from all_users order by username";
     if(args.length > 0) sqlcmd = args[0];
     Oracle or = new Oracle("amoghtan", "ros14490"); // use your own userid/pass
     if(or.sysError)
     {
         System.err.println("Login Error.");
         System.exit(1);
     }
     else
     {
         String arr[] = or.doSql(sqlcmd);
         if(or.sysError)
         {
             System.err.println("Error in the query.");
             for(String s: arr) System.out.println(s);
             System.exit(2);
         }
         else
         {
             for(String s: arr) System.out.println(s);
         }
     }
   }
}
