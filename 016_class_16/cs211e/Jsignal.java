import sun.misc.Signal;
import sun.misc.SignalHandler;

public class Jsignal implements SignalHandler
{

    private SignalHandler oldHandler;

//*******************************install()********************************
    public static SignalHandler install(String signalName)
    {
        Signal diagSignal = new Signal(signalName);
        Jsignal instance = new Jsignal();
        instance.oldHandler = Signal.handle(diagSignal, instance);
        return instance;
    }

//********************************handle()********************************
    public void handle(Signal signal)
    {
        System.out.println("Signal handler called for signal " + signal);
        try 
        {

            signalAction(signal);

            // Chain back to previous handler, if one exists
            if (oldHandler != SIG_DFL && oldHandler != SIG_IGN)
            {
                oldHandler.handle(signal);
            }

        } 
        catch (Exception e) 
        {
            System.out.println("handle|Signal handler failed, reason " +
            e.getMessage());
            e.printStackTrace();
        }
    }

//*****************************signalAction()*****************************
    public void signalAction(Signal signal)
    {
        System.out.println("Handling " + signal.getName());
        System.out.println("Just sleep for 5 seconds.");
        try
        {
            Thread.sleep(5000);
        }
        catch (InterruptedException e)
        {
            System.out.println("Interrupted: " + e.getMessage());
        }
    }

//*********************************main()*********************************
    public static void main(String[] args)
    {
        // to catch SYGTERM, SIGINT, SIGABRT signals
        Jsignal.install("TERM");
        Jsignal.install("INT");
        Jsignal.install("ABRT");

        System.out.println("Signal handling example.");
        try
        {
            Thread.sleep(50000);
        }
        catch (InterruptedException e)
        {
            System.out.println("Interrupted: " + e.getMessage());
        }

    }
}
