/*
 Author: Edgar Saavedra
 Date  : 2/21/16
 Program Name: USDB.java
 Homework: 2
 Objective: This program shows how to
 connect to oracle/mysql databases and 
 perform CRUD operations using SQL commands.
*/
 import java.io.*;
 import java.lang.reflect.*;
 import java.sql.*;
 import java.util.*;

 public class USDB 
 {

 	static boolean debug_mode = (System.getProperties().getProperty("debug") != null);

	 //****************************debug()*****************************
 	public static void debug(Object o) 
 	{
 		if(debug_mode)
 		{
 			System.out.println(""+o);
 		}
 	}
 	
	 //****************************USStateParser*****************************
 	private static class USStateParser
 	{
 		private static String fileName;
		 //****************************USStateParser()*****************************
 		public USStateParser(String filePath)
 		{
 			fileName = filePath;
 		}
 		
		 //****************************parseUSStates()*****************************
 		private void parseUSStates(ArrayList<String> states, ArrayList<String> capitals)
 		{
 			try
 			{
 				Scanner sc = new Scanner(new File(fileName));
 				String line;
 				int i = 0;
 				
 				sc.nextLine(); sc.nextLine();

 				while(sc.hasNext())
 				{
 					line = sc.nextLine();
 					String temp[] = line.split("\\s\\s+");
 					if(temp.length >= 2)
 					{
 						if(temp.length == 2) 
 						{
 							states.add(temp[0]);
 							capitals.add(temp[1]);
 						}
 						else
 						{
 							states.add(temp[0] + " " + temp[1]);
 							capitals.add(temp[2]);
 						}
 					}
 				}
 				
 			}catch(FileNotFoundException e){System.err.println(e);}
 		}

		 //****************************parseUSStatesArr()*****************************
 		public Map parseUSStatesArr()
 		{
 			Map collection = new TreeMap<>();
 			ArrayList<String> states   = new ArrayList<>();
 			ArrayList<String> capitals = new ArrayList<>();

 			parseUSStates(states, capitals);
 			
 			collection.put("states", states);
 			collection.put("capitals", capitals);
 			
 			return collection;
 		}
 	} 

	 //****************************main()*****************************
 	public static void main(String[] args) 
 	{

 		try 
 		{

    		// get properties
 			Properties p = System.getProperties();
 			
 			
 			Connection con = null;
 			DBConnector connection = null;
 			
 			USStateParser parser = null;

 			ArrayList<String> states   = null;
 			ArrayList<String> capitals = null;
 			
    	    // chec if oracle or mysql
 			if(p.getProperty("dbType") != null)
 			{
    			//mysql
 				if(p.getProperty("dbType").equals("mysql"))
 					parser = new USStateParser("/Users/edgar/Google Drive/CCSF-Edgar/2016/spring/cs211e_advanced_java_ee/Homework2/src/US_states");
 				
    			//oracle
 				if(p.getProperty("dbType").equals("oracle"))
 					parser = new USStateParser("/students/esaaved2/public_html/cs211e/US_states");
 				
    			//make sure we have Parser to process our data
 				if(parser != null)
 				{
    				//porcess our state data
 					Map collection = parser.parseUSStatesArr();
 					collection = parser.parseUSStatesArr();
 					capitals = (ArrayList<String>) collection.get("capitals");
 					states   = (ArrayList<String>) collection.get("states");
 					
        			//Handle our wanted connections
 					connection = new DBConnector(p.getProperty("dbType"));
 				}
 			}
 			
 			if(connection != null)
 			{
    			//start a connection
 				con = connection.startCon();

                //create our table
 				if(p.getProperty("createTable") != null)
 					connection.createTable(con);
        		//populate table
 				if(p.getProperty("populate") != null)
 					connection.populateTables(states, capitals, con);
 				
        		//get count
 				Integer sTCount = connection.getTableCount("States", con);
 				System.out.println("State Table count: "+sTCount);
 				
        		//Get state/capital data
 				System.out.println("Get capital where state is washington: "+connection.getValueWhere(con,"States", "capital", "state", "Washington"));
 				System.out.println("Get state where capital is Albany: "+connection.getValueWhere(con,"States", "state", "capital", "Albany"));
 				System.out.println("Get capital where state is Illinois: "+connection.getValueWhere(con,"States", "capital", "state", "Illinois"));
 				
        		//drop table
 				if(p.getProperty("dropTable") != null)
 					connection.dropTables(con, "States");
 				
        		// close the connection
 				con.close();
 			}
 			
 		} catch (Exception e) 
 		{
 			e.printStackTrace();
 		}
 	}
 	
 }
