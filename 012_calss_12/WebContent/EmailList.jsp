<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Email List Application</title>
</head>
<body>
 <h1>Join our email list</h1>
 <p>To join our email list, enter your name and email address below
 <br> then click on the submit button.
 </p>
 
 <form action="show_email_entry.jsp" method="get">
   <table cellspacing="5" border="0">
    <tr>
     <td align="right">First Name</td>
     <td align="right"><input type="text" name="firstName"></td>
    </tr>
    <tr>
     <td align="right">Last Name</td>
     <td align="right"><input type="text" name="lastName"></td>
    </tr>
    <tr>
     <td align="right">Email</td>
     <td align="right"><input type="text" name="email"></td>
    </tr>
    <tr>
     <td><br><input type="submit" value="Submit"></td>
    </tr>
   </table>
 </form>
</body>
</html>