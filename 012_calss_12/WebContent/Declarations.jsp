<%@page import="java.util.Calendar"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
 <!--
    This is a declaration
   -->
 <%! 
 public java.util.Date printDate()
 {
	 return new java.util.Date(); 
 }
 %>
 <h2>The current date is <%= printDate() %></h2>
 
 <%!
  java.util.Calendar c = Calendar.getInstance();
  int day = c.get(c.DAY_OF_WEEK);
 %>
 
 <h3>The day is: <em><%= day %></em></h3>
 <% 
 if(day==1 || day == 7)
 { 
 %>
  <p><Font color="red" size="+1">it is weekend</Font></p>
 <% 
  } else 
  { 
 %> 
  <p><Font color="blue" size="+1">it is week day</Font></p>
 <% 
 }
 %>
	<% 
	 for(int fontSize = 1; fontSize <= 5; fontSize++)
	 {
	%>
	  <font color="green" size="<%= fontSize %>">I love JSP</font><br>
	<%
	 }
	%>   
 
</body>
</html>