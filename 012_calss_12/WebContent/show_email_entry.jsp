<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
 <% 
  String firstname = request.getParameter("firstName"); 
	 String lastName = request.getParameter("lastName"); 
	 String email = request.getParameter("email"); 
 %>
 <h1>Thanks for joining our email list</h1>
 <p>here is the information that you entered:</p>
 <table cellspacing="5" cellpadding="5" border="1">
  <tr>
   <td align="right"><%= firstname %></td>
  </tr>
  <tr>
   <td align="right"><%= lastName %></td>
  </tr>
  <tr>
   <td align="right"><%= email %></td>
  </tr>
 </table>
 <p>To enter another email address, click on the back button
 in your browser or return button shown below</p>
 <form action="EmailList.jsp" method="post">
   <input type="submit" value="Return">
 </form>
</body>
</html>