/*
 Author: Edgar Saavedra
 Date  : 2/16/15
 Program Name: MiniDns.java
 Homework: 1
 Objective: This program shows the creation
 	of a server that translates IP addresses 
 	to domains and domains to IP addresses
*/
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MiniDns 
{
	/******* MiniDnsConverter *******/
	public static class MiniDnsConverter implements Runnable
	{
		protected Socket socket;
		private static final String DOMAIN_PATTERN = 
				"^((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}$";
		private static final String IP_PATTERN = 
				"^(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})$";	
		
		/******* MiniDnsConverter() *******/
		MiniDnsConverter(Socket s)
		{
			socket = s;
		}
		
		/******* isIP() *******/
		private boolean isIP(String addr)
		{
			
			Pattern p = Pattern.compile(IP_PATTERN);
			Matcher m = p.matcher(addr);
			return m.find();
		}
		
		/******* convert() *******/
		private String convert(String ip)
		{

			try {
				InetAddress ipParser = InetAddress.getByName(ip);
				String h;
				if(isIP(ip))
					h = ipParser.getHostName();
				else
					h = ipParser.getHostAddress();
				return h;
			} catch (UnknownHostException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return "";
		}
		
		/******* run() *******/
		@Override
		public void run() {
			//get data from the user
			try(InputStream in = socket.getInputStream())
			{
				try(OutputStream out = socket.getOutputStream())
				{
					PrintWriter pw = new PrintWriter(out,true);
					
					//get the input stream and pass it to scanner
					//this is like System.in
					try(Scanner s = new Scanner(in))
					{
						// true in constructor means flush data out
						boolean done = false;
						//loop on this connection till user 
						// decides to end connection
						while(!done && s.hasNextLine())
						{
							String line = s.nextLine();
							if(line.trim().equals("close"))
								done = true;
							else
							{
								pw.println(convert(line));
							}
								
						}
						pw.close();
						s.close();
						socket.close();
					}catch(Exception e)
					{
						
					}
				}catch(IOException e)
				{
					
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/******* main() *******/
	public static void main(String[] args) 
	{
		ServerSocket ss;
		//save our port to the property files
		String prefferredPort = "16038";
		String portNumStr = System.getProperty("CS211PORT",prefferredPort);
		
		if(portNumStr == null)
		{
			System.setProperty("CS211PORT",prefferredPort);
			portNumStr = System.getProperty("CS211PORT",prefferredPort);
		}
			
		//default port number
		int port = Integer.parseInt(prefferredPort);
		//if we have a port
		port = Integer.parseInt(portNumStr);
		try
		{
			ss = new ServerSocket(port);
			//infinite loop
			while(true)
			{
				Socket newConnection = ss.accept();
				MiniDnsConverter converterService = new MiniDnsConverter(newConnection);
				Thread t = new Thread(converterService);
				t.start();
					
			}
		}catch(IOException e)
		{
			
		}
	}

}
