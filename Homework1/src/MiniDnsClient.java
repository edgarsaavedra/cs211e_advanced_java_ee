/*
 Author: Edgar Saavedra
 Date  : 2/16/15
 Program Name: MiniDnsClient.java
 Homework: 1
 Objective: This program shows the use of 
 	implementing a client that can connect to a server
*/
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class MiniDnsClient 
{
	/******* getPort() *******/
	public static Integer getPort()
	{
		String systemPort = System.getProperty("CS211PORT");
		if(systemPort != null)
		{
			Integer i = Integer.parseInt(systemPort);
			 if(i.intValue() > 0) return i ;
		}
		return 16038;
	}
	
	/******* main() *******/
	public static void main(String[] args) 
	{
		String str;
		try {
			Socket s = new Socket("localhost",getPort());
			System.out.println("Connected to ReverseServer host "+s.getInetAddress());
			Scanner sc = new Scanner(s.getInputStream());//read from socket
			//true: flush out channels of memory right away, write to socket
			PrintWriter pw = new PrintWriter(s.getOutputStream(),true);
			//read from standard input
			Scanner sc1 = new Scanner(System.in);
			while(true)
			{
				//prompt of system
				System.out.print("Enter Domain or IP address# ");
				// to imideatly print out our prompt and it will let you 
				// know that it is not our client with the issue
				System.out.flush(); 
				if(sc1.hasNext())
				{
					//get user input
					str = sc1.nextLine();
					if(str.equals("close"))
					{
						sc.close();
						pw.close();
						sc1.close();
						s.close();
						System.exit(0); 
					}
					else
					{
						//send to server
						pw.println(str);
						//get server response, and read response
						System.out.println(sc.nextLine());
					}
				}
			}
			
			
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
