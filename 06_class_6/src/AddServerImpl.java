import java.net.MalformedURLException;
import java.rmi.*;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.*;

public class AddServerImpl extends UnicastRemoteObject implements AddServerInft 
{
  protected AddServerImpl() throws RemoteException {
		super();
	}
  @Override
  public double add(double a, double b) throws RemoteException {
	return (a+b);
  }
    
  public static void main(String[] args) 
  {
	  if(System.getSecurityManager() == null)
	  {
		  System.setSecurityManager(null);
	  }
	  try
	  {
         AddServerImpl asi = new AddServerImpl();
         //create a server
         //Rebind is placing our methods into
         //registery, this is saying that all methods
         //in the interface should be registered
         LocateRegistry.createRegistry(6600);
         Naming.rebind("rmi://localhost:6600/Addserver", asi);
         System.out.println("AddServer Ready");
	  }catch(RemoteException e){
		  e.printStackTrace();
	  } 
	  catch (MalformedURLException e) {
		e.printStackTrace();
	  }
	  
  }

}
