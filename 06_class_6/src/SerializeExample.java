import java.io.*;


public class SerializeExample {
	public static void main(String[] args) {
		MyClass mc = new MyClass(8899);
		
		//serialize object
		try {
			ObjectOutputStream oos;
			oos = new ObjectOutputStream(
					new FileOutputStream("/Users/edgar/Desktop/serial"));
			oos.writeObject(mc);
			oos.flush();
			oos.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		//read object
		try
		{
			MyClass mc1;
			ObjectInputStream ois = new 
					ObjectInputStream(
							new FileInputStream("/Users/edgar/Desktop/serial"));
			mc1 = (MyClass)ois.readObject();
			ois.close();
			System.out.println(""+mc1.getI());
		}catch(Exception e)
		{
			
		}
		System.exit(0);
		
	}
	public static class MyClass implements Serializable
	{
		private int i;
		public MyClass(int x)
		{
			i = x;
		}
		public int getI()
		{
			return i;
		}
	}
}
