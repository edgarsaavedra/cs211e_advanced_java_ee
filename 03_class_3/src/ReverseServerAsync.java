import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ReverseServerAsync 
{
	public static String reverse(String s)
	{
//		String: immutable, when you make string and change it, it will discard it
//		StringBuffer: Original Is changed, it is Thread safe, meaning other threads cannot interfeer until it is finished
//		StringBuilder: Original Is changed, is not Thread Safe, async it is not safe
		
		/*
		 * StringBuilder is faster than stringBuffer, it does not have the overhead of synchronization
		 * if only thread, StringBuilder is prefect,
		 * 
		 * if wokring in multi-thread enviroment then StringBuffer is best
		 * */
		StringBuffer sb = new StringBuffer(s);
		return (new String(sb.reverse()));
	}
	public static class ClientHandler implements Runnable
	{
		protected ServerSocket ss;
		public ClientHandler(ServerSocket st)
		{
			ss = st;
		}
		@Override
		public void run() {
			String str;
			Socket s;
			try {
				s = ss.accept();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			//normally Scanner is simplest approach
			Scanner sc;
			try {
				sc = new Scanner(s.getInputStream());
				// true means immediately throw the output
				PrintWriter pw = new PrintWriter(s.getOutputStream(),true);
				while(sc.hasNext())
				{
					str = sc.nextLine();
					if(str.equals("exit"))
						break;
					else
						pw.println(reverse(str));
				}
				sc.close();
				pw.close();
				s.close();//close socket
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	public static void main(String[] args) 
	{
		String str;
		ServerSocket ss;
		try {
			//get for me port or set default
			// an approach to get data using get/set property
			String portNumStr = System.getProperty("port","5678");
			int port = 5678;
			try
			{
				port = Integer.parseInt(portNumStr);
				ss = new ServerSocket(port);
				for(;;) // infinite loop
				{
					Socket s = ss.accept();
					ClientHandler ch = new ClientHandler(s);
					Thread t = new Thread(ch);
					t.start();
				}
				
			}catch(Exception e)
			{
				System.out.println("invalid port number "+portNumStr);
				System.exit(0);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
