import java.net.*;
import java.util.Scanner;
import java.io.*;
public class ReverseServer {

	public static String reverse(String s)
	{
//		String: immutable, when you make string and change it, it will discard it
//		StringBuffer: Original Is changed, it is Thread safe, meaning other threads cannot interfeer until it is finished
//		StringBuilder: Original Is changed, is not Thread Safe, async it is not safe
		
		/*
		 * StringBuilder is faster than stringBuffer, it does not have the overhead of synchronization
		 * if only thread, StringBuilder is prefect,
		 * 
		 * if wokring in multi-thread enviroment then StringBuffer is best
		 * */
		StringBuffer sb = new StringBuffer(s);
		return (new String(sb.reverse()));
	}
	
	public static void main(String[] args) 
	{
		String str;
		ServerSocket ss;
		try {
			ss = new ServerSocket(5678);
			//single thread
			Socket s = ss.accept();
			//normally Scanner is simplest approach
			Scanner sc = new Scanner(s.getInputStream());
			// true means immediately throw the output
			PrintWriter pw = new PrintWriter(s.getOutputStream(),true);
			while(sc.hasNext())
			{
				str = sc.nextLine();
				if(str.equals("exit"))
					break;
				else
					pw.println(reverse(str));
			}
			sc.close();
			pw.close();
			s.close();//close socket
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
