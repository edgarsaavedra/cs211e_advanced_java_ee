import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;


public class ReverseClient {

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		String str;
//		if(args.length == 0) die("usage: java ReverseClient server-address");
		if(args.length == 0)
		{
			System.out.println("usage: java ReverseClient server-address");
			System.exit(0);
		};
		//if we know port number
		try
		{
			Socket ss = new Socket(args[0],5678);
			//make sure to write lots of prints to make sure that each
			//step is correct
			System.out.println("Connected to ReverseServer host "+ss.getInetAddress());
			Scanner sc = new Scanner(ss.getInputStream());//read from socket
			//write to socket
			//true: flush out channels of memory right away
			PrintWriter pw = new PrintWriter(ss.getOutputStream(),true);
			
			//read from standard input
			Scanner sc1 = new Scanner(System.in);
			while(true)
			{
				System.out.println("# ");//prompt of system
				System.out.flush(); // to imideatly print out our prompt and it will let you know that it is not our client with the issue
				if(sc1.hasNext())
				{
					//get user input
					str = sc1.nextLine();
					if(str.equals("exit"))
					{
						sc.close();
						pw.close();
						sc1.close();
						ss.close();
						System.exit(0); 
					}
					else
					{
						//send to server
						pw.println(str);
						//get server response, and read response
						System.out.println(sc.nextLine());
					}
				}
			}
//			sc.close();
//			pw.close();
//			sc1.close();
//			ss.close();
//			Socket s  = new Socket(args[0],0);
//			int port = s.getPort();
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

}
