import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class IcreamShop extends HttpServlet
{
	@Override
	protected void doPost(HttpServletRequest req,
	    HttpServletResponse res) throws ServletException, IOException
	{
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		String msg = req.getParameter("name");
		String result = req.getParameter("password");
		pw.println("<html><head><title>Ice Cream Servlet</title></head");
		pw.println("<body><h1><strong>hi"+msg);
		pw.println("</strong></h1><h3>");
		
		result = req.getParameter("flavor");
		
		pw.println("Got your order for "+result+" ice cream ");
		String kinds[] = req.getParameterValues("toppings");
		if(kinds == null) pw.println("with no toppings");
		else
		{
			pw.println("with <ul>");
			for(int i = 0; i<kinds.length;i++)
			{
				pw.println("<li>"+kinds[i]);
				pw.println("</li></ul>");
			}
		}
		result = req.getParameter("place");
		if(result.equals("Eat here"))
			pw.println("to eat here");
		else
			pw.println("to go");
		pw.println("</h3></body></html>");
		pw.close();
	}
}
