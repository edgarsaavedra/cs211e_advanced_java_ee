import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PostOrGetOrder extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest req,
	    HttpServletResponse res) throws ServletException, IOException
	{
		String order = req.getParameter("Order");
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		pw.println("<head><title>Ger Order Served</title></head>");
		pw.println("<body><h1><strong>I'd like to order <br>");
		pw.println(order+"</strong></h1></body>");
		pw.close();
		
	}
	@Override
	protected void doPost(HttpServletRequest req,
	    HttpServletResponse res) throws ServletException, IOException
	{
		doGet(req,res);
	}

}
