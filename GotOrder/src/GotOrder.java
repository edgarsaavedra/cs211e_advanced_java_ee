import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GotOrder extends HttpServlet
{
	@Override
	protected void doGet(HttpServletRequest req,
	    HttpServletResponse res) throws ServletException, IOException
	{
		String order = req.getParameter("Order");
		res.setContentType("text/html");
		PrintWriter pw = res.getWriter();
		pw.println("<head><title>Ger Order Served</title></head>");
		pw.println("<body><h1><strong>I'd like to order <br>");
		pw.println(order+"</strong></h1></body>");
		pw.close();
		
	}

}
